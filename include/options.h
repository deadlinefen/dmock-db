#pragma once
#include "exports.h"
#include <string>

namespace DB_NAME {

struct LogOpt {
  int daily_hour = 2;
  int daily_min = 30;
  spdlog::level::level_enum level = spdlog::level::debug;
  spdlog::level::level_enum flush_on = spdlog::level::err;
  std::string pattern = "[%Y-%m-%d %H:%M:%S] [%l] [%n] [tid: %t] - <%@ - %!>, %v";
};

struct MemOpt {
  size_t cache_size = 64 * MB;
};

struct Options {
  std::string db_name;
  LogOpt log_opt;
};

}