#pragma once

#include <spdlog/spdlog.h>

#define DB_NAME DMockDB

//
#define KB 1024
#define MB 1024 * KB
#define GB 1024 * MB

//
#define LIKELY(x)   (__builtin_expect((x), 1))
#define UNLIKELY(x) (__builtin_expect((x), 0))

//
#define OP_FAIL(opr) UNLIKELY(RC::SUCCESS != (ret = opr))
#define OP_SUCC(opr) LIKELY(RC::SUCCESS == (ret = opr))
#define IS_SUCC LIKELY(RC::SUCCESS == ret)
#define IS_FAIL UNLIKELY(RC::SUCCESS != ret)
#define IS_NULL(ptr) UNLIKELY(nullptr == (ptr))
#define IS_INITED LIKELY(is_inited_)
#define INITED_TWICE UNLIKELY(is_inited_)

// log
#define LOG_INFO(x, ...) log_->info(x, ##__VA_ARGS__)
#define LOG_WARN(x, ...) log_->warn(x, ##__VA_ARGS__)
#define LOG_ERROR(x, ...) log_->error(x, ##__VA_ARGS__)
#define LOG_CRITICAL(x, ...) log_->critical(x, ##__VA_ARGS__)

#define GLOB_LOG_INFO(x, ...) spdlog::info(x, ##__VA_ARGS__)
#define GLOB_LOG_WARN(x, ...) spdlog::warn(x, ##__VA_ARGS__)
#define GLOB_LOG_ERROR(x, ...) spdlog::error(x, ##__VA_ARGS__)
#define GLOB_LOG_CRITICAL(x, ...) spdlog::critical(x, ##__VA_ARGS__)

//
#define INTERNAL_BUF_COPY \
inline void copy_to_buffer(const char* data, size_t size)  \
{                                                          \
  memcpy(buf_ + pos_, data, size);                         \
  pos_ += size;                                            \
}                                               
