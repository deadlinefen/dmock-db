#pragma once

#include "rc.h"
#include "options.h"

namespace DB_NAME {

class DB {
 public:
  DB();
  DB(const DB&) = delete;
  DB& operator=(const DB&) = delete;
  virtual ~DB();

  static RC Open(DB *&db_ptr, Options *options);
  static RC Close();
};

}