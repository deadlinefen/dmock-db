#pragma once

typedef enum {
  SUCCESS,
  FAIL,
  INIT_TWICE,
  UNINITED,
  OPEN_FAILED,
  IO_ERROR,
  SYNC_ERROR,
  OUT_OF_MEM
} RC;

static const char *const rc_str[] = {
  "SUCCESS",
  "FAIL",
  "INIT_TWICE",
  "UNINITED",
  "OPEN_FAILED",
  "IO_ERROR",
  "SYNC_ERROR",
  "OUT_OF_MEM"
};

#define KR(x) rc_str[x]