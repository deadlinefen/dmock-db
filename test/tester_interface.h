#pragma once
#include <spdlog/spdlog.h>
#include <cassert>

namespace dmock_test {

class Tester {
  public:
    void Test() { Init(); Run(); Close(); }
    virtual void Run() = 0;
    virtual void Init() = 0;
    virtual void Close() = 0;
};

}