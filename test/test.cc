#include <spdlog/sinks/stdout_color_sinks.h>

#include "writable_file_test.h"
#include "write_batch_test.h"
#include "../util/coding.h"

using namespace dmock_test;

int main() {

  auto console = spdlog::stdout_color_st("test");
  spdlog::set_level(spdlog::level::info);
  spdlog::set_pattern("[%Y-%m-%d %H:%M:%S] [%l] %v");

  {
    struct stat st;
    if (stat("output", &st) || !S_ISDIR(st.st_mode)) {
      system("mkdir output");
    }
  }

  GLOB_LOG_INFO("---Writable File Test---");
  {
    WritableFileTest tester;
    tester.Test();
  }
  GLOB_LOG_INFO("Module pass!\n");
  
  GLOB_LOG_INFO("---coding Test---");
  {
    uint32_t t = 114514;
    GLOB_LOG_INFO("origin num={}.", t);
    char s[10] = {0};
    const char *header = s;
    int size = EncodeVarInt(s, t);
    GLOB_LOG_INFO("[test.1] Encode over. size={}", size);
    uint32_t dt = DecodeVarInt<uint32_t>(header);
    GLOB_LOG_INFO("[test.1] Decode over, decode_num={}, dec={}", dt, header - s);
  }
  GLOB_LOG_INFO("Module pass!\n");

  GLOB_LOG_INFO("---Write Batch Test---");
  {
    WriteBatchTester tester;
    tester.Test();
  }
  GLOB_LOG_INFO("Module pass!\n");
  
  spdlog::drop_all();
  return 0;
}
