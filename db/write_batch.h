#pragma once

#include "exports.h"
#include "sview.h"
#include "../util/coding.h"

namespace DB_NAME {

static const size_t kHeader = 12;
enum ValueType {kTypeDeletion = 0x0, kTypeValue = 0x1 };
struct BatchHeader {
  uint64_t sequence;
  uint32_t count;
  const char data[0];
};

class WriteBatch {
  
 public:
  WriteBatch();
  WriteBatch(const WriteBatch&) = default;
  WriteBatch& operator=(const WriteBatch &) = default;
  ~WriteBatch() = default;

  void Put(const SView &key, const SView &value);
  void Delete(const SView &key);
  void Clear();
  void Append(const WriteBatch *src);
  SView Contents() { return SView(rep_); }
  inline uint32_t GetCount() const { return ((BatchHeader *)rep_.data())->count; }
  inline void SetSequence(uint64_t seq) { *(uint64_t *)rep_.data() = seq; }

 private:
  uint64_t get_sequence() { return *(uint64_t *)rep_.data(); }
  inline void add_count() { ++((BatchHeader *)rep_.data())->count; }
  inline void add_count(uint32_t n) {
    ((BatchHeader *)rep_.data())->count += n;
  }
  inline void set_count(uint32_t n) {
    ((BatchHeader *)rep_.data())->count = n;
  }
 private:
  std::string rep_;
};

}