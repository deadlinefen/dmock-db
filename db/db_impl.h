#pragma once

#include <mutex>

#include "db.h"
#include "writable_file.h"
#include "writers.h"

namespace DB_NAME {

class DBImpl : public DB {
 public:
  DBImpl(Options *options);
private:
  friend class DB;

  void init_logger();
  RC open_wal_file();
 private:
  Options *options_;
  std::shared_ptr<spdlog::logger> log_;
  WritableFile *wal_;
  wal::Writer *writer_;

  std::mutex mutex_;
  bool is_inited_;
};

}