#pragma once

#include "exports.h"
#include "sview.h"
#include "writable_file.h"
#include "crc32c/crc32c.h"

namespace DB_NAME {

namespace wal {

constexpr size_t kHeaderSize = 4 + 3 + 1;
constexpr size_t kBlockSize = 32 * KB;
static constexpr unsigned int kWalCrcBase[5] = {
  1383945041, 2685849682, 3007718310, 1093509285, 2514994254
};
struct EntryHeader {
  uint32_t checksum;
  union {
    struct {
      uint32_t size : 24;
      uint32_t type :  8;
    };
    uint32_t meta;
  };
};


enum RecordType {
  kTypeEmpty,
  kTypeFull,
  kTypeFirst,
  kTypeMiddle,
  kTypeLast
};


class Writer {
 public:
	explicit Writer(WritableFile *file);

  Writer(const Writer &w) = delete;
  Writer& operator=(const Writer &w) = delete;

  ~Writer() = default;

  RC AddRecord(const SView &data_view);
 private:
  RC write_entry(RecordType type, const char *data, uint32_t size);
 private:
  WritableFile *file_;
  size_t pos_;
};

}
}