#include "write_batch.h"

namespace DB_NAME {

WriteBatch::WriteBatch() : rep_(kHeader, 0) {}

void WriteBatch::Put(const SView &key, const SView &value)
{
  add_count();
  rep_.push_back(static_cast<char>(kTypeValue));
  PutLengthPrefixedSView(rep_, key);
  PutLengthPrefixedSView(rep_, value);
}

void WriteBatch::Delete(const SView &key)
{
  add_count();
  rep_.push_back(static_cast<char>(kTypeDeletion));
  PutLengthPrefixedSView(rep_, key);
}

void WriteBatch::Clear()
{
  rep_.clear();
  rep_.resize(kHeader);
}

void WriteBatch::Append(const WriteBatch *src)
{
  add_count(src->GetCount());
  rep_.append(src->rep_.data()+kHeader, src->rep_.size()-kHeader);
}

}