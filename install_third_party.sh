cd third-party
cd spdlog && mkdir build && cd build && cmake .. && make install -j8 && rm -rf build
cd ..
cd crc32c && git submodule update --init && mkdir build && cd build && cmake .. && make install -j8 && rm -rf build
